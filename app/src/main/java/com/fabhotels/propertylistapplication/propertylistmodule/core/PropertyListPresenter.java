package com.fabhotels.propertylistapplication.propertylistmodule.core;

import com.fabhotels.propertylistapplication.model.PropertyListResponse;
import com.fabhotels.propertylistapplication.realm.RealmService;
import com.fabhotels.propertylistapplication.rx.RxSchedulers;

import rx.Observer;

/**
 * Created by Niharika on 24-05-2018.
 * PropertyListPresenter
 */

public class PropertyListPresenter {

    private RxSchedulers rxSchedulers;
    private PropertyModule propertyModule;
    private PropertyListView mPropertyListView;
    private RealmService realmService;

    public PropertyListPresenter(RxSchedulers schedulers, PropertyListView propertyListView, PropertyModule propertyModule, RealmService realmService) {
        this.rxSchedulers = schedulers;
        this.mPropertyListView = propertyListView;
        this.propertyModule = propertyModule;
        this.realmService = realmService;
    }


    /**
     * Method to fetch data from server
     */
    public void loadPropertyList() {
        mPropertyListView.showWait();
        propertyModule.provideListProperty()
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.androidThread())
                .unsubscribeOn(rxSchedulers.io())
                .subscribe(new Observer<PropertyListResponse>() {
                    @Override
                    public void onCompleted() {
                        mPropertyListView.removeWait();

                    }

                    @Override
                    public void onError(Throwable e) {
                        mPropertyListView.swapAdapter(realmService.getPropertyList());
                    }

                    @Override
                    public void onNext(PropertyListResponse propertyListItems) {
                        realmService.writeData(propertyListItems.getPropertyListing());
                        mPropertyListView.swapAdapter(realmService.getPropertyList());
                    }
                });
    }
}
