package com.fabhotels.propertylistapplication.builder;


import com.fabhotels.propertylistapplication.propertylistmodule.api.PropertyListApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Niharika on 24-05-2018.
 * PropertyApiServiceModule
 */

@Module
class PropertyApiServiceModule {

    @AppScope
    @Provides
    PropertyListApi provideApiService(Retrofit retrofit) {
        return retrofit.create(PropertyListApi.class);
    }


}
