package com.fabhotels.propertylistapplication.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


import com.fabhotels.propertylistapplication.app.App;
import com.fabhotels.propertylistapplication.propertylistmodule.core.PropertyListPresenter;
import com.fabhotels.propertylistapplication.propertylistmodule.core.PropertyListView;
import com.fabhotels.propertylistapplication.propertylistmodule.dagger.DaggerPropertyListComponent;
import com.fabhotels.propertylistapplication.propertylistmodule.dagger.PropertyListModule;

import javax.inject.Inject;


/**
 * Created by Niharika on 25-05-2018.
 * PropertyListActivity
 */
public class PropertyListActivity extends AppCompatActivity {
    //  Dagger-MVP-RX
    @Inject
    PropertyListView view;
    @Inject
    PropertyListPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerPropertyListComponent.builder().appComponent(App.getNetComponent())
                .propertyListModule(new PropertyListModule(this)).build().inject(this);
        setContentView(view.getPropertyListView());
        presenter.loadPropertyList();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        view.unbind();
    }
}
